#@TYPE: Machine
#@NAME: joypinote with RaspberryPi 4 Development Board (32bit)
#@DESCRIPTION: Machine configuration for the RaspberryPi 4 in 32 bit mode

DEFAULTTUNE ?= "cortexa7thf-neon-vfpv4"
require conf/machine/include/tune-cortexa7.inc
include conf/machine/include/rpi-base.inc

PREFERRED_VERSION_linux-raspberrypi = "4.19.86"
RPI_KERNEL_DEVICETREE_OVERLAYS= ""
RPI_KERNEL_DEVICETREE = "bcm2711-rpi-4-b.dtb"
IMAGE_FSTYPES = "tar.bz2"

MACHINE_EXTRA_RRECOMMENDS += "\
    linux-firmware-rpidistro-bcm43455 \
    bluez-firmware-rpidistro-bcm4345c0-hcd \
    bootfiles \
"

SERIAL_CONSOLES ?= "115200;ttyS0"

VC4DTBO ?= "vc4-fkms-v3d"
ARMSTUB ?= "armstub7.bin"

ENABLE_SPI_BUS = "1"
ENABLE_I2C = "1"
ENABLE_UART = "1"

CMDLINE="console=serial0,115200 console=tty1 root=/dev/nfs nfsroot=192.168.0.1:/tftpboot/rootfs,vers=3 rw ip=dhcp rootwait elevator=deadline splash plymouth.ignore-serial-consoles dwc_otg.fiq_enable=0 dwc_otg.fiq_fsm_enable=0 dwc_otg.nak_holdoff=0 isolcpus=0,1"
