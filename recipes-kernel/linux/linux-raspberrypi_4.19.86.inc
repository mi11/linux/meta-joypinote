FILESEXTRAPATHS_prepend := "${THISDIR}/linux-raspberrypi:"

SRC_URI = "git://gitlab.utc.fr/mi11/linux/linux-raspberrypi.git;branch=4.19.86;rev=1115bb7a39b9d7045fe7be6186934169fc708221"
SRC_URI_append_joypinote-xenomai = " file://pre-rpi4-4.19.86-xenomai3-simplerobot.patch \
                            file://ipipe-core-4.19.82-arm-6-mod-4.49.86.patch.ipipe \
                            https://xenomai.org/downloads/xenomai/stable/xenomai-3.1.tar.bz2;name=xenomai"

SRC_URI[xenomai.sha256sum] = "6469aa40d0e61aca9342ba39e822b710177734b171e53a4ab9abb97401a2c480"

require linux-raspberrypi.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

KERNEL_EXTRA_ARGS_append_rpi = " DTC_FLAGS='-@ -H epapr'"

# Apply xenomai patch
do_patch_append_joypinote-xenomai () {
    ${WORKDIR}/xenomai-3.1/scripts/prepare-kernel.sh --arch=arm --linux=${S} --ipipe=${WORKDIR}/ipipe-core-4.19.82-arm-6-mod-4.49.86.patch.ipipe
}
