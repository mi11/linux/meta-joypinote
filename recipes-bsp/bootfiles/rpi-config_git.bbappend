# there is a 4096 byte limit for config.txt
# see https://github.com/raspberrypi/firmware/issues/1012#issuecomment-401021275
# so remove all comments from the file

do_deploy_append() {
    grep -v ^\#  ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}/config.txt | grep . > ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}/config.tmp
    mv ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}/config.tmp ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}/config.txt
}
