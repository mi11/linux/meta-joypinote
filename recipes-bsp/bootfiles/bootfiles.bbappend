#keep only necessary file for mi11
do_deploy_append() {
   
    rm ${DEPLOYDIR}/${PN}/start.elf
    rm ${DEPLOYDIR}/${PN}/start4x.elf
    rm ${DEPLOYDIR}/${PN}/start4cd.elf
    rm ${DEPLOYDIR}/${PN}/start4db.elf
    rm ${DEPLOYDIR}/${PN}/start_*.elf
    rm ${DEPLOYDIR}/${PN}/*.stamp
    rm ${DEPLOYDIR}/${PN}/*.dat
    rm ${DEPLOYDIR}/${PN}/bootcode.bin

}
